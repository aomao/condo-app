package me.aizon.app.adapter;

import java.util.List;

import me.aizon.app.MainFragmentWrapper;
import me.aizon.app.R;
import me.aizon.app.data.ParkingBay;
import me.aizon.app.fragments.RegisterFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GridItemAdapter extends BaseAdapter {
	private Context context;
	List<ParkingBay> parkingLots;

	public GridItemAdapter(Context context, List<ParkingBay> lots) {
		this.context = context;
		this.parkingLots = lots;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);
			ParkingBay rowItem = (ParkingBay) getItem(position);
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.parking_bay_item, null);

			// set value into textview
			TextView parknoTV = (TextView) gridView
					.findViewById(R.id.grid_item_parking);
			parknoTV.setText(rowItem.getParkingId());

			TextView carplateTV = (TextView) gridView
					.findViewById(R.id.grid_item_carplate);
			
			
			Log.d("AIZON", "Adapter parking lot " + rowItem.getLotStatus() );
			
			if(rowItem.getLotStatus().equals("taken")){
				gridView.setBackgroundColor(context.getResources().getColor(R.color.redbay));
				carplateTV.setText(rowItem.getCarPlate());
			}else{
				gridView.setBackgroundColor(context.getResources().getColor(R.color.greenbay));
				carplateTV.setText("");
			}

		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	@Override
	public int getCount() {
		return parkingLots.size();
	}

	@Override
	public Object getItem(int position) {
		return parkingLots.get(position);
	}

	@Override
	public long getItemId(int position) {
		return parkingLots.indexOf(getItem(position));
	}

}
