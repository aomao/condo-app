package me.aizon.app.data;

/**
 * This code encapsulates ParkingBay item data.  
 * 
 */

public class ParkingBay {

	private String parkingId;
	private String status;
	private String carPlate;

	public ParkingBay() {

	}

	public ParkingBay(String parkingId, String status) {
		this.parkingId = parkingId;
		this.status = status;
	}

	public String getParkingId() {
		return parkingId;
	}

	public void setParkingId(String parkingId) {
		this.parkingId = parkingId;
	}

	public String getCarPlate() {
		return carPlate;
	}

	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	
	public String getLotStatus() {
		return status;
	}

	public void setLotStatus(String status) {
		this.status = status;
	}
	
	/*public String getVizObjectId() {
		return objId;
	}

	public void setVizObjectId(String objId) {
		this.objId = objId;
	}*/

}
