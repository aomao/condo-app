package me.aizon.app.data;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

@ParseClassName("Visitor")
public class Visitor extends ParseObject {

	public Visitor() {
		// A default constructor is required.
	}

	public String getName() {
		return getString("name");
	}

	public void setName(String name) {
		put("name", name);
	}

	public ParseUser getAuthor() {
		return getParseUser("author");
	}

	public void setAuthor(ParseUser user) {
		put("author", user);
	}

	public String getIdno() {
		return getString("idno");
	}

	public void setIdno(String idno) {
		put("idno", idno);
	}
	
	public String getMobile() {
		return getString("mobile");
	}

	public void setMobile(String mobile) {
		put("mobile", mobile);
	}
	
	public String getUnit() {
		return getString("unit");
	}

	public void setUnit(String unit) {
		put("unit", unit);
	}
	
	public String getType() {
		return getString("type");
	}

	public void setType(String type) {
		put("type", type);
	}
	
	public String getRemarks() {
		return getString("remarks");
	}

	public void setRemarks(String remarks) {
		put("remarks", remarks);
	}
	
	public String getStatus() {
		return getString("status");
	}

	public void setStatus(String status) {
		put("status", status);
	}
	
	public String getCarplate() {
		return getString("car_plate");
	}

	public void setCarplate(String carplate) {
		put("car_plate", carplate);
	}
	
	public String getOvernight() {
		return getString("overnight");
	}

	public void setOvernight(String overnight) {
		put("overnight", overnight);
	}
	
	public String getParkingLot() {
		return getString("parking_lot");
	}

	public void setParkingLot(String parkinglot) {
		put("parking_lot", parkinglot);
	}

	public ParseFile getPhotoFile() {
		return getParseFile("photo");
	}

	public void setPhotoFile(ParseFile file) {
		put("photo", file);
	}

}
