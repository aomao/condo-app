package me.aizon.app.fragments;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.aizon.app.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class VisitorsFragment extends Fragment {
	ListView list_content, list_head;
	ArrayList<HashMap<String, String>> mylist, mylist_title;
	SimpleAdapter adapter_title, madapter;
	HashMap<String, String> map1, map2;
	private int itemPosition;
	private String visitorName, visitorId, parkLot;
	String TAG = "AIZON";
	OnEditButtonClick _mEditButtonClickListener;
	private ProgressDialog progressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ActionBar ab = getActivity().getActionBar();
		ab.setTitle("Visitors");
		View v = inflater.inflate(R.layout.visitors_fragment, null);
		find_and_modify_view(v);

		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			_mEditButtonClickListener = (OnEditButtonClick) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnEditButtonClick");
		}
	}

	private void find_and_modify_view(View v) {
		/*** get the ids ****************/
		list_content = (ListView) v.findViewById(R.id.listView2);
		list_head = (ListView) v.findViewById(R.id.listView1);

		// set headings
		set_headings();

		progressDialog = ProgressDialog.show(getActivity(), "",
				"Getting data...", true);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visitor");
		query.whereEqualTo("status", "in");
		query.orderByDescending("updatedAt");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> visitorsList, ParseException e) {
				if (e == null) {
					/*
					 * Log.d("score", "Retrieved " + parkingList.size() +
					 * " parking lots");
					 */
					mylist = new ArrayList<HashMap<String, String>>();

					for (ParseObject temp : visitorsList) {
						Format formatter = new SimpleDateFormat(
								"dd-MM-yyyy HH:mm");
						String dateS = formatter.format(temp.getUpdatedAt());

						map2 = new HashMap<String, String>();
						map2.put("visitorId", temp.getObjectId());
						map2.put("name", temp.getString("name"));
						map2.put("idno", temp.getString("idno"));
						map2.put("mobile", temp.getString("mobile"));
						map2.put("unit", temp.getString("unit"));
						map2.put("parking", temp.getString("parking_lot"));
						map2.put("timein", dateS);
						mylist.add(map2);
						// Close progress dialog
						progressDialog.dismiss();
					}
					try {
						madapter = new SimpleAdapter(getActivity(), mylist,
								R.layout.visitors_list_item, new String[] {
										"name", "idno", "mobile", "unit",
										"parking", "timein" }, new int[] {
										R.id.vnameTV, R.id.idTV, R.id.mobileTV,
										R.id.unitTV, R.id.parkingTV,
										R.id.timeinTV });
						list_content.setAdapter(madapter);
					} catch (Exception er) {

					}

				} else {
					Log.d("score", "Error: " + e.getMessage());
				}
			}
		});

		// button click listener
		list_content
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// ModelProduct product = (ModelProduct)
						// parent.getItemAtPosition(position);
						itemPosition = position;
						Log.d("list visitors", "Position: " + itemPosition);
						visitorName = mylist.get(position).get("name");
						visitorId = mylist.get(position).get("visitorId");
						parkLot = mylist.get(position).get("parking");
						showAlertDialog(visitorName, visitorId, parkLot,
								position);
					}
				});

	}

	private void set_headings() {
		mylist_title = new ArrayList<HashMap<String, String>>();

		/********** Display the headings ************/

		map1 = new HashMap<String, String>();

		map1.put("name", "Name");
		map1.put("idno", "ID");
		map1.put("mobile", "Mobile");
		map1.put("unit", "Unit");
		map1.put("parking", "Parking");
		map1.put("timein", "Time in");
		mylist_title.add(map1);

		try {
			adapter_title = new SimpleAdapter(getActivity(), mylist_title,
					R.layout.visitors_list_header, new String[] { "name",
							"idno", "mobile", "unit", "parking", "timein",
							"signout" }, new int[] { R.id.vnameTV, R.id.idTV,
							R.id.mobileTV, R.id.unitTV, R.id.parkingTV,
							R.id.timeinTV });
			list_head.setAdapter(adapter_title);
		} catch (Exception e) {

		}

		/********************************************************/

	}

	public void showAlertDialog(String visitorName, final String visitorId,
			final String parkingLot, final int itemPosition) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		// set title
		alertDialogBuilder.setTitle("Visitor: " + visitorName);

		// set dialog message
		alertDialogBuilder
				.setMessage("Select action for visitor!")
				.setCancelable(true)
				.setPositiveButton("Sign out",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// signout user by updating visitor entry
								signoutVisitor(visitorId, parkingLot,
										itemPosition);

							}
						})
				.setNegativeButton("Edit",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Here you send some data over MainActivity to
								// the other Fragment

								_mEditButtonClickListener
										.onEditButton(visitorId);
								// open visitor editing form
								// Create new fragment and transaction
								Fragment register = new RegisterFragment();
								// consider using Java coding conventions (upper
								// first char class names!!!)
								FragmentTransaction transaction = getFragmentManager()
										.beginTransaction();
								// Replace whatever is in the fragment_container
								// view with this fragment,
								// and add the transaction to the back stack
								transaction.replace(R.id.frgmCont, register);
								//transaction.addToBackStack(null);

								// Commit the transaction
								transaction.commit();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	// an interface which will use to comunicate with your activity
	public interface OnEditButtonClick {
		public void onEditButton(String visitorID);
	}

	public void signoutVisitor(String visitorId2, final String parkingLot,
			final int itemPosition) {
		// show Loading Dialog while getting data from external database
		progressDialog = ProgressDialog.show(getActivity(), "",
				"Signing out visitor...", true);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visitor");

		// Retrieve the object by id
		query.getInBackground(visitorId2, new GetCallback<ParseObject>() {
			public void done(ParseObject visitor, ParseException e) {
				if (e == null) {
					// Now let's update it with some new data. In this case,
					// only cheatMode and score
					// will get sent to the Parse Cloud. playerName hasn't
					// changed.
					visitor.put("status", "out");
					visitor.saveInBackground(new SaveCallback() {
						public void done(ParseException e) {
							if (e == null) {
								// Update successfully.
								// display in long period of time
								Toast.makeText(getActivity(),
										"Visitor signed out", Toast.LENGTH_LONG)
										.show();
								// remove visitor from list
								mylist.remove(itemPosition);
								madapter.notifyDataSetChanged();
								setParkingLotAsFree(parkingLot);
							} else {
								// The save failed.
								Log.d(TAG, "User update error: " + e);
							}
							// Close progress dialog
							progressDialog.dismiss();
						}
					});
				}
			}
		});

	}

	protected void setParkingLotAsFree(String parkLot) {
		if (parkLot == null) {
			// no need to update
			Log.d(TAG, "Parking lot not entered ");
		} else {
			// visitor has taken parking lot
			// update the Parking lot table
			ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkingLot");
			query.whereEqualTo("parking_lot", parkLot);
			// Retrieve the object by id
			query.getFirstInBackground(new GetCallback<ParseObject>() {
				public void done(ParseObject parkinglot, ParseException e) {
					if (parkinglot != null) {
						// Now let's update it with some new data. Data
						// will get sent to the Parse Cloud.
						parkinglot.put("status", "free");
						parkinglot.saveInBackground(new SaveCallback() {
							public void done(ParseException e) {
								if (e == null) {
									// Saved successfully.
									Log.d(TAG, "Parkinglot set free!");
								} else {
									// The save failed.
									Log.d(TAG, "Parkinglot update error: " + e);

								}
							}

						});

					}
				}

			});
		}

	}

}