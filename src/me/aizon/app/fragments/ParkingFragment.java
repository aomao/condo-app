package me.aizon.app.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import me.aizon.app.R;
import me.aizon.app.adapter.GridItemAdapter;
import me.aizon.app.data.ParkingBay;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class ParkingFragment extends Fragment {

	private ProgressDialog progressDialog;
	HashMap<String, String> map1;
	List<ParkingBay> parkList;
	// Used to reference item
	private ParkingBay currentItem;
	GridView gridView;
	private ArrayAdapter<String> visitorsArray;
	private List<String> tempArrList;
	ArrayList<HashMap<String, String>> myArrList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ActionBar ab = getActivity().getActionBar();
		ab.setTitle("Parking Bays");
		View v = inflater.inflate(R.layout.parking_fragment, null);
		gridView = (GridView) v.findViewById(R.id.parkingGrid);
		find_and_modify_view(v);

		return v;
	}

	private void find_and_modify_view(View v) {
		
		//get current visitors data
		getCurrentVisitorsWithCars();
		
		

	}

	protected void getCurrentVisitorsWithCars() {

		progressDialog = ProgressDialog.show(getActivity(), "",
				"Getting data...", true);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visitor");
		query.whereEqualTo("status", "in");
		query.whereExists("parking_lot");		
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> visitorsList, ParseException e) {
				if (e == null) {
					/*
					 * Log.d("score", "Retrieved " + parkingList.size() +
					 * " parking lots");
					 */
					//tempArrList = new ArrayList<String>();
					myArrList = new ArrayList<HashMap<String, String>>();
					map1 = new HashMap<String, String>();

					for (ParseObject temp : visitorsList) {
						//tempArrList.add(temp.getString("parking_lot"));
						map1.put(temp.getString("parking_lot"), temp.getString("car_plate"));
						//myArrList.add(map1);
					}
					
					getParkingLots();

				} else {
					Log.d("AIZON", "Error: " + e.getMessage());
				}
				// Close progress dialog
				progressDialog.dismiss();
			}
		});
		
	}

	protected void getParkingLots() {
		parkList = new ArrayList<ParkingBay>();
		

		ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkingLot");
		query.orderByAscending("parking_lot");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> parkingLots, ParseException e) {
				if (e == null) {
					/*
					 * Log.d("score", "Retrieved " + parkingList.size() +
					 * " parking lots");
					 */

					for (ParseObject temp : parkingLots) {
						currentItem = new ParkingBay();
						currentItem.setParkingId(temp.getString("parking_lot"));
						currentItem.setLotStatus(temp.getString("status"));
						currentItem.setCarPlate(map1.get(temp.getString("parking_lot")));
						parkList.add(currentItem);
						Log.d("AIZON", "parking lot " + temp.getString("status") );
					}
					
					// process data in custom adapter

					gridView.setAdapter(new GridItemAdapter(getActivity(),
							parkList));

				} else {
					Log.d("AIZON", "Error: " + e.getMessage());
				}

				
			}
		});
		
	}
}