package me.aizon.app.fragments;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import me.aizon.app.MainFragmentWrapper;
import me.aizon.app.R;
import me.aizon.app.data.Visitor;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterFragment extends Fragment {
	private Button regButton;
	private Spinner blockSpinner, sectionSpinner, flatSpinner,
			overnightSpinner, parkinglotSpinner;
	private View layoutView;
	private ParseImageView imageHolder;

	private AutoCompleteTextView idnoAutoComplete = null;
	private EditText name, type, remarks, carplate, mobile;

	private TextView optionalData;
	private Boolean hideOptionData = false, setCameraImage = false;
	private LinearLayout optionalDataSection;
	private String visitorId;
	String TAG = "AIZON";

	private String tName, tMobile, tIdno, tType, tRemarks, tUnit, tBlock,
			tSection, tFlat, tOvernight, tParkingLot, tCarplate,
			CurrParkingLot;
	private ArrayAdapter<CharSequence> blockAdapter, sectionAdapter,
			flatAdapter, overnightAdapter;
	private ArrayAdapter<String> parkingLotAdapter, idnosAdapter;
	private List<String> list, idNoList;
	private ProgressDialog progressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ActionBar ab = getActivity().getActionBar();
		ab.setTitle("Register visitor");
		layoutView = inflater.inflate(R.layout.register_fragment, null);
		register_layout_items(layoutView);
		getIdNumbersForAutoComplete(layoutView);
		// get visitorId from Main activity
		MainFragmentWrapper activity = (MainFragmentWrapper) getActivity();
		visitorId = activity.getMyData();
		Log.d(TAG, "passed data: " + visitorId);
		if (visitorId.equals("0")) {
			setup_layout_for_new_visitor(layoutView);

		} else {
			setup_layout_for_visitor_editing(layoutView, visitorId);
		}

		return layoutView;
	}

	private void getIdNumbersForAutoComplete(View v) {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visitor");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> visitorList, ParseException e) {
				if (e == null) {
					idNoList = new ArrayList<String>();

					for (ParseObject temp : visitorList) {
						idNoList.add(temp.getString("idno"));
					}

					idnosAdapter = new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_list_item_1, idNoList);

					idnoAutoComplete = (AutoCompleteTextView) layoutView
							.findViewById(R.id.idnoAutoCompleteTV);

					idnoAutoComplete.setAdapter(idnosAdapter);

				} else {
					Log.d("score", "Error: " + e.getMessage());
				}
			}
		});

	}

	private void setup_layout_for_new_visitor(View mv) {

		regButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				saveVisitorToDb(v);
			}
		});

		// fill Data From Db To ParkingSpinner with default selection
		fillDataFromDbToParkingSpinner(mv, "0");

	}

	private void setup_layout_for_visitor_editing(View mv, String visitorID) {

		regButton.setText("Update");

		regButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateVisitorInDb(v);
			}
		});

		progressDialog = ProgressDialog.show(getActivity(), "",
				"Getting data...", true);
		// Log.d(TAG, "worked: 0 " + visitorID );
		// get user data from db
		ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("Visitor");
		userQuery.getInBackground(visitorID, new GetCallback<ParseObject>() {
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					idnoAutoComplete.setText(object.getString("idno"));
					name.setText(object.getString("name"));
					mobile.setText(object.getString("mobile"));
					type.setText(object.getString("type"));
					remarks.setText(object.getString("remarks"));
					carplate.setText(object.getString("car_plate"));
					tUnit = object.getString("unit");
					tOvernight = object.getString("overnight");
					tParkingLot = CurrParkingLot = object
							.getString("parking_lot");
					setSpinnersSelections(tUnit, tOvernight, tParkingLot);

					// check if visitor got Parking lot
					if (tParkingLot != null && !tParkingLot.isEmpty()) {
						Log.d(TAG, "tParkingLot not null");
						// open optional data section
						hideOptionData = false;
						optionalDataSection.setVisibility(View.VISIBLE);
					} else {
						Log.d(TAG, "tParkingLot is null: " + tParkingLot);
					}
					// Log.d(TAG, "worked: 1" );
				} else {
					// something went wrong
					// Log.d(TAG, "error: 1" );
				}
				// Close progress dialog
				progressDialog.dismiss();
			}
		});

		// don't load image from db when returning from Camera fragment
		if (!setCameraImage) {
			// load visitor photo from Parse.com
			loadVisitorPhoto(visitorID);
		}

	}

	private void loadVisitorPhoto(String visitorID) {
		// ParseImageView imageView = (ParseImageView)
		// findViewById(android.R.id.icon);
		/*
		 * progressDialog = ProgressDialog.show(getActivity(), "",
		 * "Downloading Image...", true);
		 */

		// Locate the class table named "Visitor" in Parse.com
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Visitor");

		// Locate the objectId from the class
		query.getInBackground(visitorID, new GetCallback<ParseObject>() {

			public void done(ParseObject object, ParseException e) {
				// check if we have photo in Cloud
				if (object.containsKey("photo")) {
					// Locate the column named "ImageName" and set
					// the string
					ParseFile fileObject = (ParseFile) object.get("photo");

					fileObject.getDataInBackground(new GetDataCallback() {

						public void done(byte[] data, ParseException e) {
							if (e == null) {
								Log.d("test", "We've got data in data.");
								// Decode the Byte[] into Bitmap
								Bitmap bmp = BitmapFactory.decodeByteArray(
										data, 0, data.length);

								// Set the Bitmap into the
								// ImageView
								imageHolder.setImageBitmap(bmp);

								// Close progress dialog
								// progressDialog.dismiss();

							} else {
								Log.d("test",
										"There was a problem downloading the data.");
							}
						}
					});
				} else {
					Log.d("test", "No photo for this visitor.");
				}
			}
		});

	}

	private void fill_with_visitor_data(View mv, final String visitorIDnumber) {

		regButton.setText("Update");

		regButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateVisitorInDb(v);
			}
		});
		Log.d(TAG, "worked: 0 " + visitorIDnumber);
		// get user data from db
		ParseQuery<ParseObject> mQuery = ParseQuery.getQuery("Visitor");
		mQuery.whereEqualTo("idno", visitorIDnumber);
		mQuery.getFirstInBackground(new GetCallback<ParseObject>() {
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					name.setText(object.getString("name"));
					mobile.setText(object.getString("mobile"));
					type.setText(object.getString("type"));
					remarks.setText(object.getString("remarks"));
					carplate.setText(object.getString("car_plate"));
					tUnit = object.getString("unit");
					tOvernight = object.getString("overnight");
					tParkingLot = object.getString("parking_lot");
					setSpinnersSelections(tUnit, "0", "0");

					// load visitor photo from Parse.com
					loadVisitorPhoto(object.getObjectId());
				} else {
					// something went wrong
					// Log.d(TAG, "error: 1" );
				}
			}
		});
	}

	protected void updateVisitorInDb(View v) {
		assignValuesFromElements();

		tBlock = tBlock.replace("Block ", "");

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Visitor");

		// Retrieve the object by id
		query.getInBackground(visitorId, new GetCallback<ParseObject>() {
			public void done(ParseObject visitor, ParseException e) {
				if (e == null) {
					Visitor mVisitor = ((MainFragmentWrapper) getActivity())
							.getCurrentVisitor();
					// Now let's update it with some new data. Data
					// will get sent to the Parse Cloud.
					visitor.put("name", tName);
					visitor.put("mobile", tMobile);
					visitor.put("idno", tIdno);
					visitor.put("unit", tBlock + "-" + tSection + "-" + tFlat);
					visitor.put("type", "visitor");
					visitor.put("remarks", tRemarks);
					// car related data
					visitor.put("car_plate", tCarplate);
					visitor.put("parking_lot", tParkingLot);
					visitor.put("overnight", tOvernight);
					visitor.put("status", "in");
					// set photo for update
					ParseFile photoFile = ((MainFragmentWrapper) getActivity())
							.getCurrentVisitor().getPhotoFile();
					if (photoFile != null) {
						visitor.put("photo", photoFile);
					}
					visitor.saveInBackground(new SaveCallback() {
						public void done(ParseException e) {
							if (e == null) {
								// Saved successfully.
								Log.d(TAG, "Visitor updated!");
								// display in long period of time
								Toast.makeText(getActivity(),
										"Visitor updated", Toast.LENGTH_LONG)
										.show();
								// check if parking lot is changing
								if (!CurrParkingLot.equals(tParkingLot)) {
									//parking lot is changed, then lets release old parking lot
									setParkingLotAsFree(CurrParkingLot);
								}
								setParkingLotAsTaken(tParkingLot);
								// open Visitors list
								redirectToVisitorsList();
							} else {
								// The save failed.
								Log.d(TAG, "User edit error: " + e);
								Toast.makeText(getActivity(),
										"User edit error", Toast.LENGTH_LONG)
										.show();
							}
						}

					});

				}
			}
		});

	}

	protected void saveVisitorToDb(View v) {
		assignValuesFromElements();

		tBlock = tBlock.replace("Block ", "");
		Visitor visitor = ((MainFragmentWrapper) getActivity())
				.getCurrentVisitor();

		visitor.setName(tName);
		visitor.setMobile(tMobile);
		visitor.setIdno(tIdno);
		visitor.setUnit(tBlock + "-" + tSection + "-" + tFlat);
		visitor.setType("visitor");
		visitor.setRemarks(tRemarks);
		visitor.setCarplate(tCarplate);
		visitor.setParkingLot(tParkingLot);
		visitor.setOvernight(tOvernight);
		visitor.setStatus("in");

		// If the user added a photo, that data will be
		// added in the CameraFragment

		// before save new visitor, check if ID number already exists
		Boolean IdNoIsUnique = checkIdNumberForUniqueness(tIdno);

		if (IdNoIsUnique) {
			// Save the visitor and return
			visitor.saveInBackground(new SaveCallback() {

				@Override
				public void done(ParseException e) {
					if (e == null) {
						// Saved successfully.
						Log.d(TAG, "New Visitor saved!");
						// display in long period of time
						Toast.makeText(getActivity(), "Visitor saved",
								Toast.LENGTH_LONG).show();
						
						setParkingLotAsTaken(tParkingLot);
						
						// open Visitors list
						redirectToVisitorsList();
					} else {
						Toast.makeText(getActivity().getApplicationContext(),
								"Error saving: " + e.getMessage(),
								Toast.LENGTH_SHORT).show();
					}
				}

			});
		} else {
			// ID number already existing
			Toast.makeText(getActivity().getApplicationContext(),
					"ID number already existing!", Toast.LENGTH_SHORT).show();
		}

	}

	private Boolean checkIdNumberForUniqueness(String idnumber) {

		Boolean idNumberIsUnique = true;
		for (String s : idNoList) {
			if (s.equals(idnumber)) {
				idNumberIsUnique = false;
			}
		}

		return idNumberIsUnique;
	}

	protected void setParkingLotAsTaken(String parkLot) {
		if (parkLot == null) {
			// no need to update
			Log.d(TAG, "Parking lot not entered ");
		} else {
			// visitor has taken parking lot
			// update the Parking lot table
			ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkingLot");
			query.whereEqualTo("parking_lot", parkLot);
			// Retrieve the object by id
			query.getFirstInBackground(new GetCallback<ParseObject>() {
				public void done(ParseObject parkinglot, ParseException e) {
					if (parkinglot != null) {
						// Now let's update it with some new data. Data
						// will get sent to the Parse Cloud.
						parkinglot.put("status", "taken");
						parkinglot.saveInBackground(new SaveCallback() {
							public void done(ParseException e) {
								if (e == null) {
									// Saved successfully.
									Log.d(TAG, "Parking lot updated!");
								} else {
									// The save failed.
									Log.d(TAG, "Parking lot update error: " + e);

								}
							}

						});

					}
				}

			});
		}

	}

	protected void setParkingLotAsFree(String parkLot) {
		if (parkLot == null) {
			// no need to update
			Log.d(TAG, "Parking lot not entered ");
		} else {
			// visitor has taken parking lot
			// update the Parking lot table
			ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkingLot");
			query.whereEqualTo("parking_lot", parkLot);
			// Retrieve the object by id
			query.getFirstInBackground(new GetCallback<ParseObject>() {
				public void done(ParseObject parkinglot, ParseException e) {
					if (parkinglot != null) {
						// Now let's update it with some new data. Data
						// will get sent to the Parse Cloud.
						parkinglot.put("status", "free");
						parkinglot.saveInBackground(new SaveCallback() {
							public void done(ParseException e) {
								if (e == null) {
									// Saved successfully.
									Log.d(TAG, "Parkinglot set free!");
								} else {
									// The save failed.
									Log.d(TAG, "Parkinglot update error: " + e);

								}
							}

						});

					}
				}

			});
		}

	}

	// here comes the value from the other fragment
	public void setData(String data) {
		optionalData.setText("data");
	}

	private void assignValuesFromElements() {
		tName = name.getText().toString();
		tMobile = mobile.getText().toString();
		tIdno = idnoAutoComplete.getText().toString();
		tType = type.getText().toString();
		tRemarks = remarks.getText().toString();
		tBlock = blockSpinner.getSelectedItem().toString();
		tSection = sectionSpinner.getSelectedItem().toString();
		tFlat = flatSpinner.getSelectedItem().toString();
		tOvernight = overnightSpinner.getSelectedItem().toString();
		tParkingLot = parkinglotSpinner.getSelectedItem().toString();
		tCarplate = carplate.getText().toString();
	}

	/*
	 * protected void setSpinnersSelections(String unitString) { String[] parts
	 * = unitString.split("-"); // set spinners selections int spinnerPosition =
	 * blockAdapter.getPosition("Block " + parts[0]);
	 * blockSpinner.setSelection(spinnerPosition);
	 * 
	 * int pos = sectionAdapter.getPosition(parts[1]);
	 * sectionSpinner.setSelection(pos);
	 * 
	 * int pos2 = flatAdapter.getPosition(parts[2]);
	 * flatSpinner.setSelection(pos2); }
	 */

	protected void setSpinnersSelections(String unitString,
			String overnightString, String parklotString) {
		// get parking lots from db and setup spinner selection
		fillDataFromDbToParkingSpinner(layoutView, parklotString);

		// check for Unit data
		if (unitString == null || unitString.isEmpty()||unitString.equals("--")) {
			// no unit data entered
		} else {
			String[] parts = unitString.split("-");
			// set spinners selections
			int spinnerPosition = blockAdapter.getPosition("Block " + parts[0]);
			blockSpinner.setSelection(spinnerPosition);

			int pos = sectionAdapter.getPosition(parts[1]);
			sectionSpinner.setSelection(pos);

			int pos2 = flatAdapter.getPosition(parts[2]);
			flatSpinner.setSelection(pos2);

			// Car parking spinners
			int pos3 = overnightAdapter.getPosition(overnightString);
			overnightSpinner.setSelection(pos3);
		}

	}

	private void register_layout_items(View mv) {

		idnoAutoComplete = (AutoCompleteTextView) mv
				.findViewById(R.id.idnoAutoCompleteTV);
		name = (EditText) mv.findViewById(R.id.nameET);
		mobile = (EditText) mv.findViewById(R.id.mobileET);
		type = (EditText) mv.findViewById(R.id.typeET);
		remarks = (EditText) mv.findViewById(R.id.remarksET);
		optionalData = (TextView) mv.findViewById(R.id.optionalDataTV);
		carplate = (EditText) mv.findViewById(R.id.carplateET);
		optionalDataSection = (LinearLayout) mv
				.findViewById(R.id.optionalDataSection);
		// Hide option data section
		optionalDataSection.setVisibility(View.GONE);
		regButton = (Button) mv.findViewById(R.id.regVisitorBtn);

		idnoAutoComplete.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long rowId) {
				String selectioIdNo = (String) parent
						.getItemAtPosition(position);
				Log.d(TAG, "selected: " + selectioIdNo);
				// we found this visitors idNo in our database, lets fill his
				// old data
				fill_with_visitor_data(layoutView, selectioIdNo);
				// close keyboard
				closeKeyboard();
			}
		});

		// imageHolder = ((ImageView) mv.findViewById(R.id.visitorImage));
		imageHolder = (ParseImageView) mv
				.findViewById(R.id.visitor_photo_preview);
		imageHolder.setPlaceholder(getResources().getDrawable(
				R.drawable.no_image));
		// imageHolder.setImageResource(R.drawable.ic_launcher);
		imageHolder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(name.getWindowToken(), 0);
				startCamera();
			}
		});

		// Fill block spinner with data from resources
		blockSpinner = (Spinner) mv.findViewById(R.id.blockSpinner);
		blockAdapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.blocks, android.R.layout.simple_spinner_item);
		blockAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		blockSpinner.setAdapter(blockAdapter);

		// Fill section spinner with data from resources
		sectionSpinner = (Spinner) mv.findViewById(R.id.sectionSpinner);
		sectionAdapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.sections, android.R.layout.simple_spinner_item);
		sectionAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sectionSpinner.setAdapter(sectionAdapter);

		// Fill flat spinner with data from resources
		flatSpinner = (Spinner) mv.findViewById(R.id.flatSpinner);
		flatAdapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.flats, android.R.layout.simple_spinner_item);
		flatAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		flatSpinner.setAdapter(flatAdapter);

		// Fill flat spinner with data from resources
		overnightSpinner = (Spinner) mv.findViewById(R.id.overnightSpinner);
		overnightAdapter = ArrayAdapter.createFromResource(getActivity(),
				R.array.overnight, android.R.layout.simple_spinner_item);
		overnightAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		overnightSpinner.setAdapter(overnightAdapter);

		optionalData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideOptionData) {
					optionalDataSection.setVisibility(View.GONE);
				} else {
					optionalDataSection.setVisibility(View.VISIBLE);
				}
				hideOptionData = !hideOptionData;
			}
		});

	}

	private void fillDataFromDbToParkingSpinner(View mv, final String parklot) {
		// show Loading Dialog while getting data from external database
		/*
		 * progressDialog = ProgressDialog.show(getActivity(), "",
		 * "Getting data...", true);
		 */

		// fill Parking lot spinner with data from db
		parkinglotSpinner = (Spinner) mv.findViewById(R.id.parkinglotSpinner);
		ParseQuery<ParseObject> query = ParseQuery.getQuery("ParkingLot");
		query.whereEqualTo("status", "free");
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> parkingList, ParseException e) {
				if (e == null) {
					list = new ArrayList<String>();
					list.add("");
					list.add(parklot);
					for (ParseObject temp : parkingList) {
						list.add(temp.getString("parking_lot"));
					}
					parkingLotAdapter = new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_spinner_item, list);
					parkingLotAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					parkinglotSpinner.setAdapter(parkingLotAdapter);

					// set selected parklot
					if (parklot.equals("0")) {
						// no need for setting selection in spinner
					} else {
						// setup selection for Parking lot
						int pos4 = parkingLotAdapter.getPosition(parklot);
						parkinglotSpinner.setSelection(pos4);
					}
					// Log.d(TAG, "parklot: " + parklot);

				} else {
					Log.d("score", "Error: " + e.getMessage());
				}

				// Close progress dialog
				// progressDialog.dismiss();

			}
		});

	}

	private void redirectToVisitorsList() {
		// reset photo file
		resetPhotoFile();

		Fragment visitors = new VisitorsFragment();
		// consider using Java coding conventions (upper first char class
		// names!!!)
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.frgmCont, visitors);
		//transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();

	}

	private void resetPhotoFile() {
		// Locate the image in res > drawable-hdpi
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.no_image);
		// Convert it to byte
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Compress image to lower quality scale 1 - 100
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] image = stream.toByteArray();
		// Create the ParseFile
		ParseFile file = new ParseFile("visitor_photo.jpg", image);
		((MainFragmentWrapper) getActivity()).getCurrentVisitor().setPhotoFile(
				file);

	}

	/*
	 * All data entry about a Meal object is managed from the
	 * MainFragmentWrapper. When the user wants to add a photo, we'll start up a
	 * custom CameraFragment that will let them take the photo and save it to
	 * the Visitor object owned by the MainFragmentWrapper. Create a new
	 * CameraFragment, swap the contents of the fragmentContainer, then add the
	 * RegisterFragment to the back stack so we can return to it when the camera
	 * is finished.
	 */
	public void startCamera() {
		setCameraImage = true;
		Fragment cameraFragment = new CameraFragment();
		FragmentTransaction transaction = getActivity().getFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.frgmCont, cameraFragment);
		transaction.addToBackStack("RegisterFragment");
		transaction.commit();

	}

	/*
	 * On resume, check and see if a meal photo has been set from the
	 * CameraFragment. If it has, load the image in this fragment and make the
	 * preview image visible.
	 */
	@Override
	public void onResume() {
		super.onResume();
		ParseFile photoFile = ((MainFragmentWrapper) getActivity())
				.getCurrentVisitor().getPhotoFile();
		if (photoFile != null && setCameraImage) {
			Log.d(TAG, "photo set: ");
			imageHolder.setParseFile(photoFile);
			imageHolder.loadInBackground(new GetDataCallback() {
				@Override
				public void done(byte[] data, ParseException e) {
					// photoPreview.setVisibility(View.VISIBLE);
					setCameraImage = false;
				}
			});
		}
	}

	// Hide keyboard on navigation away
	@Override
	public void onPause() {
		super.onPause();
		// close keyboard when switching to other page
		closeKeyboard();

	}

	private void closeKeyboard() {
		InputMethodManager inputManager = (InputMethodManager) ((MainFragmentWrapper) getActivity())
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		// check if no view has focus:
		View v = ((MainFragmentWrapper) getActivity()).getCurrentFocus();
		if (v == null)
			return;

		inputManager.hideSoftInputFromWindow(v.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

	}

}
