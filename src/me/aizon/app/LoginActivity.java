package me.aizon.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseAnalytics;

public class LoginActivity extends Activity {

	private EditText username = null;
	private EditText password = null;
	private TextView attempts;
	private Button login;
	int counter = 3;
	private String EXTRA = "page";

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		username = (EditText) findViewById(R.id.editText1);
		password = (EditText) findViewById(R.id.editText2);
		login = (Button) findViewById(R.id.button1);

		ParseAnalytics.trackAppOpened(getIntent());

		/*
		 * ParseObject testObject = new ParseObject("TestObject");
		 * testObject.put("foo", "bar 2"); testObject.saveInBackground();
		 */
	}

	public void login(View view) {
		if (username.getText().toString().equals("admin")
				&& password.getText().toString().equals("password")) {
			/*Toast.makeText(getApplicationContext(), "Redirecting...",
					Toast.LENGTH_SHORT).show();*/
			redirectUserToVisitorsPage();
		} else {
			Toast.makeText(getApplicationContext(), "Wrong Credentials",
					Toast.LENGTH_SHORT).show();

		}

	}
	
	public void redirectUserToVisitorsPage() {
	    Intent intent = new Intent(this, MainFragmentWrapper.class);
	    //String pageName = "visitors";
	    //intent.putExtra(EXTRA, pageName);
	    startActivity(intent);
	}
}
