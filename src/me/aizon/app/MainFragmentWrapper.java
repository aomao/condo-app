package me.aizon.app;

import me.aizon.app.data.Visitor;
import me.aizon.app.fragments.ParkingFragment;
import me.aizon.app.fragments.RegisterFragment;
import me.aizon.app.fragments.VisitorsFragment;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainFragmentWrapper extends Activity implements VisitorsFragment.OnEditButtonClick{
	RegisterFragment register;
	VisitorsFragment visitors;
	ParkingFragment parking;
	FragmentTransaction fTrans;
	private String visitorId = "0";
	private Visitor visitor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		visitor = new Visitor();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_fragments_holder);
		
		//Intent intent = getIntent();
	    //String pageName = intent.getStringExtra("page");
	    openVisitorsPage();
	}
	
	public void sendDataToFrag1(String data){
		RegisterFragment Obj=(RegisterFragment) getFragmentManager().findFragmentById(R.id.frgmCont);
        Obj.setData(data);
    }

	// if the wizard generated an onCreateOptionsMenu you can delete
	// it, not needed for this tutorial

	public void onClick(View v) {
		//create new fragment for Register screen
		register = new RegisterFragment();
		visitors = new VisitorsFragment();
		parking = new ParkingFragment();
		
		fTrans = getFragmentManager().beginTransaction();
		switch (v.getId()) {
		case R.id.register:
			visitorId = "0";
			fTrans.replace(R.id.frgmCont, register);
			break;
		case R.id.visitors:
			fTrans.replace(R.id.frgmCont, visitors);
			break;
		case R.id.parking:
			fTrans.replace(R.id.frgmCont, parking);
			break;
		default:
			break;
		}
		//fTrans.addToBackStack(null);
		fTrans.commit();
	}
	
	//Data for Register Fragment
	public String getMyData() {
        return visitorId;
    }

	//assign passed variable from Visitor fragments
	@Override
	public void onEditButton(String visitorID) {
		visitorId = visitorID;
	}
	
	public Visitor getCurrentVisitor() {
		return visitor;
	}
	
	public void openVisitorsPage(){
		visitors = new VisitorsFragment();
		fTrans = getFragmentManager().beginTransaction();
		fTrans.replace(R.id.frgmCont, visitors);
		fTrans.addToBackStack(null);
		fTrans.commit();
	}
}
