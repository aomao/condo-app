package me.aizon.app;

import me.aizon.app.data.Visitor;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.app.Application;

public class VMSApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		/*
		 * we'll subclass ParseObject for convenience to
		 * create and modify Visitor objects
		 */
		ParseObject.registerSubclass(Visitor.class);
		// Add your initialization code here
		Parse.initialize(this, "f0A8fmQZSUNNHH3ZTZogXStgJOYZWwHkFlH9L8V1", "HJUglk8eP8ugBfoO0WSaAFc9pUahpmD07ZtkxHKP");


		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicWriteAccess(true);
		defaultACL.setPublicReadAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
		
	}

}
